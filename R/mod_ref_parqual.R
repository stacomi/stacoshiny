#' ref_parqual UI Function
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd
#'
#' @importFrom shiny NS tagList
mod_ref_parqual_ui <- function(id) {
  ns <- NS(id)
  tagList(
    # to have popup message on error
    spsDepend("toastr"),
    fluidRow(
      column(width = 8,
        selectizeInput(
          ns("select_ref_parqual"),
          h5("Choisissez une ou des caract\u00e9ristique(s) qualitative(s) de lot :"),
          selected = NULL,
          choices = NULL,
          multiple = TRUE)
      ),
      column(width = 4,
        textOutput(ns("test_parqual")),
        actionButton(inputId = ns("button_ref_parqual"),
          label = "OK")
      )
    )
  )
}

#' ref_parqual Server Functions
#'
#' @noRd
mod_ref_parqual_server <- function(id, DD, mytab) {
  moduleServer(id, function(input, output, session) {
    ns <- session$ns
    observeEvent(eventExpr = {
      DD$button_ref_stage_migr_car()
      DD$button_quan_to_qual()
      DD$tabs()
    },
    handlerExpr = {
      req(DD$tabs() == mytab)
      shinyCatch(
        {
          validate(need(exists("envir_stacomi"), "Le programme stacomi doit être lancé"))
          db_connection <- envir_stacomi$db_connection
          validate(need(!is.null(db_connection), "db needs connection"))

          ref_dc <- rlang::env_get(envir_stacomi, "ref_dc", default = NULL)
          ref_taxa <- rlang::env_get(envir_stacomi, "ref_taxa", default = NULL)
          ref_stage <- rlang::env_get(envir_stacomi, "ref_stage", default = NULL)
          ref_parqual <- rlang::env_get(envir_stacomi, "ref_parqual", default = NULL)
          if (!is.null(ref_dc)) {
            if (length(ref_dc@dc_selected) > 0) {
              if (!is.null(ref_taxa)) {
                if (length(ref_taxa@taxa_selected) > 0) {
                  if (!is.null(ref_stage)) {
                    if (length(ref_stage@stage_selected) > 0) {
                      if (is.null(ref_parqual)) {
                        # parqual does not exist so we create it
                        # and the selected value is NULL
                        ref_parqual <- new("ref_parqual")
                        shinybusy::show_modal_spinner(text = "loading from db")
                        ref_parqual <- charge_with_filter(ref_parqual,
                          dc_selected = ref_dc@dc_selected,
                          taxa_selected = ref_taxa@taxa_selected,
                          stage_selected = ref_stage@stage_selected)
                        shinybusy::remove_modal_spinner()
                        assign("ref_parqual", ref_parqual, envir = envir_stacomi)
                        updateSelectizeInput(session,
                          "select_ref_parqual",
                          choices = ref_parqual@data$par_nom,
                          selected = NULL)
                      } else {
                        # if a parqual was already present we want to use the values already selected even in another report
                        # there is a special handling of parm created by quan_to_qual
                        shinybusy::show_modal_spinner(text = "loading from db")
                        suppl <- ref_parqual@data[grepl("discrete", ref_parqual@data$par_code), ]
                        ref_parqual <- charge_with_filter(ref_parqual,
                          dc_selected = ref_dc@dc_selected,
                          taxa_selected = ref_taxa@taxa_selected,
                          stage_selected = ref_stage@stage_selected)
                        ref_parqual@data <- rbind(ref_parqual@data, suppl)
                        shinybusy::remove_modal_spinner()
                        par_selected <- ref_parqual@data[ref_parqual@data$par_code %in% ref_parqual@par_selected, "par_nom"]
                        suppl1 <- ref_parqual@par_selected[grepl("discrete", ref_parqual@par_selected)]
                        # on ne charge le complément que si le paramètre n'a pas été généré par quant to qual
                        if (length(par_selected) > 0 & length(suppl1) == 0) {
                          ref_parqual <- charge_complement(ref_parqual) # this is an additional load for qualitative parm
                        }
                        assign("ref_parqual", ref_parqual, envir = envir_stacomi)
                        updateSelectizeInput(session,
                          "select_ref_parqual",
                          choices = ref_parqual@data$par_nom,
                          selected = par_selected)
                        output$test_parqual <- renderText(envir_stacomi$ref_parqual@par_selected)
                      }
                    } else {
                      warning("Pas de stade sélectionné")
                    } # end else ref_stage
                  }
                } # else {
                # warning("Pas de taxon sélectionné")
                # } # end else ref_taxa
              }
            } else {
              warning("Pas de dc sélectionné")
            } # end length(ref_dc@dc_selected
          } # end if is.null(ref_dc)

        },
        blocking_level = "error")
    }, ignoreInit = TRUE, ignoreNULL = FALSE)

    observeEvent(input$button_ref_parqual, ignoreInit = TRUE, {
      shinyCatch(
        {
          # Valide les données et écrit dans l'environnement stacomi
          ref_parqual <- rlang::env_get(envir_stacomi, "ref_parqual", default = NULL)
          validate(need(!is.null(ref_parqual), "ref_parqual is not in envir_stacomi"))
          if (!is.null(input$select_ref_parqual)) {
            par_code_selected <- ref_parqual@data[ref_parqual@data$par_nom %in% input$select_ref_parqual, "par_code"]
            ref_parqual <- choice_c(ref_parqual, par_code_selected)
            assign("ref_parqual", ref_parqual, envir = envir_stacomi)
            output$test_parqual <- renderText(envir_stacomi$ref_parqual@par_selected)
          } else {
            ref_parqual@par_selected <- character(0)
            assign("ref_parqual", ref_parqual, envir = envir_stacomi)
            output$test_parqual <- renderText(NULL)
          }
        },
        blocking_level = "error")
    })
    return(reactive(input$button_ref_parqual))
  })
}


## To be copied in the UI
# mod_ref_parqual_ui("ref_parqual_ui_1")

## To be copied in the server
# mod_ref_parqual_server("ref_parqual_ui_1")
