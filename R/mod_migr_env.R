#' migr_env UI Function
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd
#'
#' @importFrom shiny NS tagList
#' @importFrom shinydashboard tabItem box
mod_migr_env_ui <- function(id) {
  ns <- NS(id)
  tabItem(tabName = "migr_env",
    box(title = "S\u00e9lections :",
      collapsible = TRUE,
      width = 3,
      mod_ref_stationmesure_ui("ref_stationmesure_mod_migr_env"),
      dateInput(ns("migr_env.datedebut"), label = h5("Choisissez une date de début :"), value = paste0(as.numeric(strftime(Sys.Date(),"%Y"))-1,"-01-01")),
      dateInput(ns("migr_env.datefin"), label = h5("Choisissez une date de fin :"),  value =paste0(as.numeric(strftime(Sys.Date(),"%Y"))-1,"-12-31")),
      mod_ref_taxa_ui("ref_taxa_mod_migr_env"),
      mod_ref_stage_ui("ref_stage_mod_migr_env"),

      checkboxGroupInput(ns("choix_sorties"), label = h4("Choisissez les sorties graphiques ou tableaux :"),
        choices = list("plot_migr_env" = 1),
        selected = 1),
      actionBttn(
        inputId = ns("bttn_migrenv"),
        label = "OK",
        style = "fill",
        color = "primary"
      )
    ),

  shinydashboardPlus::box(
  id=ns("box_plot_migr_env"),
  title="Plot migr env",
  status = "primary",
  solidHeader = TRUE,
  collapsible = TRUE,
  collapsed=TRUE,
  width=8,
  plotOutput(ns("plot_migr_env"),
             width = "100%",
             height = "600px")
) 

  )
}

#' migr_env Server Functions
#' @importFrom shinipsum random_ggplot
#' @noRd
mod_migr_env_server <- function(id,DD,mytab) {
  moduleServer(id, function(input, output, session) {
    ns <- session$ns
    observeEvent(
      eventExpr = {
        # DD$button_ref_stationmesure_mod_migr_env()
        # DD$button_ref_taxa_migr_env()
        # DD$button_ref_stage_migr_env()
        input$bttn_migrenv
      },
      handlerExpr = {
        shinyCatch({
          validate(need(exists("envir_stacomi"), "Le programme stacomi doit être lancé"))
          db_connection <- envir_stacomi$db_connection
          validate(need(!is.null(db_connection), "db needs connection"))
          r_mig_env<-new("report_mig_env")
          ref_dc <- base::get("ref_dc", envir=envir_stacomi)
          ref_taxa <- base::get("ref_taxa", envir=envir_stacomi)
          ref_stage <- base::get("ref_stage", envir=envir_stacomi)
          isolate(ref_env <- rlang::env_get(envir_stacomi, "ref_env", default = NULL))
          req(!is.null(ref_env))
          validate(need(length(ref_env@env_selected) > 0, "Les stations de mesures n'ont pas été sélectionnées"))
          validate(need(length(ref_dc@dc_selected)>0, "Pas de DC sélectionné"))										
          validate(need(length(ref_taxa@taxa_selected)>0, "pas de taxon sélectionné"))
          validate(need(length(ref_stage@stage_selected)>0, "pas de stade sélectionné"))
        
          r_mig_env <- choice_c(r_mig_env,
                              dc=ref_dc@dc_selected,
                              taxa=ref_taxa@taxa_selected,
                              stage=ref_stage@stage_selected,
                              stationMesure=ref_env@env_selected,
                              datedebut=input$migr_env.datedebut,
                              datefin=input$migr_env.datefin,
                              silent=TRUE)	
          shinybusy::show_modal_spinner(text="please wait") # show the modal window
          r_mig_env <- charge(r_mig_env) # this is necessary to load operations, DF and DC
          r_mig_env <- connect(r_mig_env)
          r_mig_env <- calcule(r_mig_env,silent=TRUE)
          shinybusy::remove_modal_spinner() # remove it when done
          
          
          # graphiques et sorties ----------------------
          if (nrow(r_mig_env@report_mig_mult@data) == 0) {
            warning("no data available_mig_mult")
          } else
          if (nrow(r_mig_env@report_env@data) == 0) {
            warning("no data available_mig_env")
          }
          else {
            if ("1" %in% input$choix_sorties) {
              if (input$box_plot_migr_env$collapsed) shinydashboardPlus::updateBox("box_plot_migr_env", action = "toggle")
              output$plot_migr_env <- renderPlot({
                color_dc <- stacomiR::colortable(color = NULL,r_mig_env@report_mig_mult@dc@dc_selected,palette = "Set2",color_function = c("random"))
                color_dc_vec <- color_dc$color
                color_dc_vec <- setNames(color_dc_vec,color_dc$name)
                
                color_sta_mesure <- stacomiR::colortable(color = NULL, r_mig_env@report_env@stationMesure@env_selected,palette = "Set2",color_function = c("random"))
                color_sta_mesure_vec <- color_sta_mesure$color
                color_sta_mesure_vec <- setNames(color_sta_mesure_vec,color_sta_mesure$name)
              
                stacomiR::plot(r_mig_env,
                     color_station = color_sta_mesure_vec,
                     color_dc = color_dc_vec
                )
                
                
              })
            } else {
              if (!input$box_plot_migr_env$collapsed) shinydashboardPlus::updateBox("box_plot_migr_env", action = "toggle")
            }  # end ifelse "1"
            
          }
        
    # output$plot_migr_env1 <- renderPlot({
    #   random_ggplot()
    # })
    # 
    # output$plot_migr_env2 <- renderPlot({
    #   random_ggplot()
    # })
        }) ## end shinycatch
  })
})}

## To be copied in the UI
# mod_migr_env_ui("migr_env_ui_1")

## To be copied in the server
# mod_migr_env_server("migr_env_ui_1")
