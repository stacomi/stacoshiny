#' The application server-side
#' 
#' @param input,output,session Internal parameters for {shiny}. 
#'     DO NOT REMOVE.
#' @import shiny
#' @noRd
app_server <- function( input, output, session ) {
  # Your application server logic 
	DD <- reactiveValues()
	mod_main_page_server("main_page_ui_1")
	DD$login_button <- mod_header_server("header_ui_1") # login_button reactive variable récupérée depuis le module
	DD$tabs <- mod_left_side_bar_server("left_side_bar_ui_1",DD)
	DD$button_ref_schema <- mod_ref_schema_server("ref_schema", DD)
	DD$button_ref_dc <- mod_ref_dc_server("ref_dc_ui_sidebar",DD)
	
	# migr_mult -------------------------------------------------
		
mod_migr_mult_server("migr_mult_ui_1",DD)
	# le mod_ref_taxa est réutilisé par les différents modules, son id (ici "ref_taxa_mod_migr_mult") change... 

	DD$button_ref_taxa_migr_mult <- mod_ref_taxa_server("ref_taxa_mod_migr_mult", DD, mytab="migr_mult") 	
	DD$button_ref_stage_migr_mult <- mod_ref_stage_server("ref_stage_mod_migr_mult", DD, mytab="migr_mult")
	DD$button_box_custom_migr_mult_step <-mod_custom_plot_server("custom_plot_migr_mult_step",DD,mytab="migr_mult")
	DD$button_box_custom_migr_mult_multiple <-mod_custom_plot_server("custom_plot_migr_mult_multiple",DD,mytab="migr_mult")
		

	# annuel ---------------------------------------------------
	
	mod_annuel_server("annuel_ui_1", DD)
	DD$button_ref_taxa_annuel <-  mod_ref_taxa_server("ref_taxa_mod_annuel", DD, mytab="annuel")
	DD$button_ref_stage_annuel <- mod_ref_stage_server("ref_stage_mod_annuel", DD, mytab="annuel")
	DD$button_box_custom_annuel_2<-mod_custom_plot_server("custom_plot_annuel_2",DD,mytab="annuel")
	
# interannuel ---------------------------------------------------	
	
  DD$button_interannuel  <- mod_interannuel_server("interannuel_ui_1",DD)
	DD$button_ref_taxa_interannuel <- mod_ref_taxa_server("ref_taxa_mod_interannuel", DD, mytab="interannuel") 	
	DD$button_ref_stage_interannuel <- mod_ref_stage_server("ref_stage_mod_interannuel", DD, mytab="interannuel")
	DD$button_box_custom_interannuel_line <- mod_custom_plot_interannual_server("custom_plot_interannuel_line",DD,mytab="interannuel", show_pas_temps=FALSE)
	DD$button_box_custom_interannuel_standard <- mod_custom_plot_server("custom_plot_interannuel_standard",DD,mytab="interannuel")
	DD$button_box_custom_interannuel_step <- mod_custom_plot_interannual_server("custom_plot_interannuel_step",DD,mytab="interannuel", show_pas_temps=FALSE)
  
  DD$button_box_custom_interannuel_barchart <- mod_custom_plot_interannual_server("custom_plot_interannuel_barchart",DD,mytab="interannuel")
  
  DD$button_box_custom_interannuel_pointrange <- mod_custom_plot_interannual_server("custom_plot_interannuel_pointrange",DD,mytab="interannuel")
	DD$button_box_custom_interannuel_density <- mod_custom_plot_server("custom_plot_interannuel_density",DD,mytab="interannuel")
	DD$button_box_custom_interannuel_seasonal <- mod_custom_plot_interannual_server("custom_plot_interannuel_seasonal",DD,mytab="interannuel", show_year_choice =FALSE)
	

	# especes ---------------------------------------------------
	
	mod_espece_server("espece_ui_1")
	DD$button_ref_taxa_espece <- mod_ref_taxa_server("ref_taxa_espece", DD, mytab="espece") 
	

	# caractéristiques de lots  ---------------------------------------------------	
	
	mod_sample_char_server("sample_char_ui_1",DD)
	DD$button_ref_taxa_sample_char <- mod_ref_taxa_server("ref_taxa_sample_char", DD, mytab="sample_char") 	
	DD$button_ref_stage_sample_char <- mod_ref_stage_server("ref_stage_sample_char", DD, mytab="sample_char")	
	DD$button_ref_par_sample_char <- mod_ref_par_server("ref_par_sample_char",DD, mytab="sample_char")
	DD$button_box_custom_sample_car_1<-mod_custom_plot_server("custom_plot_sample_char_1",DD,mytab="sample_char")
	DD$button_box_custom_sample_car_2<-mod_custom_plot_server("custom_plot_sample_char_2",DD,mytab="sample_char")
	DD$button_box_custom_sample_car_3<-mod_custom_plot_server("custom_plot_sample_char_3",DD,mytab="sample_char")
	

	# mod_migr_car ---------------------------------------------------	
	
	DD$button_quan_to_qual <- mod_migr_car_server("migr_car_ui_1", DD)
	DD$button_ref_taxa_migr_car <- mod_ref_taxa_server("ref_taxa_mod_migr_car", DD, mytab="migr_car") 	
	DD$button_ref_stage_migr_car <- mod_ref_stage_server("ref_stage_mod_migr_car", DD, mytab="migr_car")	
	DD$button_ref_parqual_migr_car <- mod_ref_parqual_server("ref_parqual_migr_car",DD, mytab="migr_car")
	DD$button_ref_parquan_migr_car <- mod_ref_parquan_server("ref_parquan_migr_car",DD, mytab="migr_car")
	DD$button_box_custom_mig_char_1<-mod_custom_plot_server("custom_plot_mig_char_1",DD,mytab="migr_car")
	DD$button_box_custom_mig_char_2<-mod_custom_plot_server("custom_plot_mig_char_2",DD,mytab="migr_car")
	DD$button_box_custom_mig_char_3<-mod_custom_plot_server("custom_plot_mig_char_3",DD,mytab="migr_car")
	
  # mod_mig_env -----------------------------
  mod_migr_env_server("migr_env_ui_1",DD)
  DD$button_ref_stationmesure_mod_migr_env <- mod_ref_stationmesure_server("ref_stationmesure_mod_migr_env", DD, mytab = "migr_env")
  DD$button_ref_taxa_migr_env <- mod_ref_taxa_server("ref_taxa_mod_migr_env", DD, mytab = "migr_env")
  DD$button_ref_stage_migr_env <- mod_ref_stage_server("ref_stage_mod_migr_env", DD, mytab = "migr_env")
  
  # mod_sat_age -----------------------------
  mod_sat_age_server("sat_age_ui_1")
  DD$button_ref_stage_sat_age <- mod_ref_stage_server("ref_stage_sat_age", DD, mytab = "sat_age")
  DD$button_ref_parquan_sat_age <- mod_ref_parquan_server("ref_parquan_sat_age", DD, mytab = "sat_age")

  # mod_ang_argentee -----------------------------

  mod_ang_argentee_server("ang_argentee_ui_1")
  mod_civ_poids_server("civ_poids_ui_1")
  
  # mod bilan_dc
  mod_bilan_dc_server("bilan_dc_ui_1",DD)
  DD$button_box_custom_bilan_dc_1<-mod_custom_plot_server("custom_plot_bilan_dc_1",DD,mytab="bilan_dc")
  DD$button_box_custom_bilan_dc_2<-mod_custom_plot_server("custom_plot_bilan_dc_2",DD,mytab="bilan_dc")
  DD$button_box_custom_bilan_dc_4<-mod_custom_plot_server("custom_plot_bilan_dc_4",DD,mytab="bilan_dc")

  # mod_bilan_df -----------------------------

  mod_bilan_df_server("bilan_df_ui_1")
  DD$button_ref_df <- mod_ref_df_server("ref_df_ui_1", DD, mytab = "bilan_df")


  # mod_env -----------------------------
  mod_env_server("env_ui_1")
  DD$button_ref_stationmesure_mod_env <- mod_ref_stationmesure_server("ref_stationmesure_mod_env", DD, mytab = "env")



}
