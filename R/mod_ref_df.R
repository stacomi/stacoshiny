#' ref_df UI Function
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd
#'
#' @importFrom shiny NS tagList
mod_ref_df_ui <- function(id) {
  ns <- NS(id)
  tagList(

    # to have popup message on error
    spsDepend("toastr"),
    selectInput(inputId = ns("select_ref_df"),
      label = "Choix du DF",
      choices = NULL,
      selected = NULL,
      multiple = FALSE
    )%>% spsComps::bsPopover(
				title = "Dispositif de franchissement",
				content = "l'ensemble des DF est montré ici, ils ne sont pas forcément rattachés au DC",
				placement = "right",
				bgcolor = "#0275d8",
				titlecolor = "white",
				contentcolor = "black",
				titlesize = "14px",
				contentsize = "12px",
				titleweight = "600",
				contentweight = "400",
				opacity = 1,
				html = FALSE,
				trigger = "hover focus"),		
    textOutput(ns("test_text")),
    actionButton(inputId = ns("button_ref_df"),
      label = "OK")

  )
}

#' ref_df Server Functions
#'
#' @noRd
mod_ref_df_server <- function(id, DD, mytab) {
  moduleServer(id, function(input, output, session) {
    ns <- session$ns
    # when observing on a button , it's good to set ignoreInit to TRUE, for dynamically created buttons

    observeEvent(eventExpr = {
      DD$login_button()
      DD$tabs()
    },
    handlerExpr = {
      req(DD$tabs() == mytab)

      shinyCatch(
        {
          # browser()
          validate(need(exists("envir_stacomi"), "Le programme stacomi doit être lancé"))
          db_connection <- envir_stacomi$db_connection
          validate(need(!is.null(db_connection), "Pas de connexion, cliquez sur le bouton dans la barre de titre"))
          shinybusy::show_modal_spinner(text = "loading from db") # show the modal window
          ref_df <- new("ref_df")
          ref_df <- charge(ref_df)
          shinybusy::remove_modal_spinner() # remove it when done
          assign("ref_df", ref_df, envir = envir_stacomi)
          updateSelectInput(session, "select_ref_df", choices = ref_df@data$df_code, selected = ref_df@data$df_code[1])


        },
        blocking_level = "error")
    },
    ignoreInit = TRUE,
    ignoreNULL = FALSE,
    priority = 0
    )

    observeEvent(input$button_ref_df,
      {

        shinyCatch(
          {

            ref_df <- rlang::env_get(envir_stacomi, "ref_df", default = NULL)
            if (!is.null(ref_df) & !is.null(input$select_ref_df)) {
              code <- ref_df@data[ref_df@data$df_code %in% input$select_ref_df, "df"]
              choice_c(ref_df, code)                # this will write in envir_stacomi

            }
          },
          blocking_level = "error")
      },
      ignoreInit = TRUE,
      ignoreNULL = TRUE,
      priority = 1
    )
    return(reactive(input$button_ref_df))


  })
}

## To be copied in the UI
# mod_ref_df_ui("ref_df_ui_1")

## To be copied in the server
# mod_ref_df_server("ref_df_ui_1")
